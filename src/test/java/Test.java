import org.ffernanl.Rover;
import org.junit.Before;

import static org.junit.Assert.assertEquals;

public class Test {

    @org.junit.Test
    public void hasDirectionNorth() {
        Rover rover = new Rover();
        assertEquals('N', rover.getRoverDirection().getDirectionAsChar());
    }

    @org.junit.Test
    public void swipeNorthDirectionToRight() {
        Rover rover = new Rover();
        rover.setRoverDirection(rover.getRoverDirection().swipeRight());
        assertEquals('E', rover.getRoverDirection().getDirectionAsChar());
    }

    @org.junit.Test
    public void swipeNorthDirectionToLeft() {
        Rover rover = new Rover();
        rover.setRoverDirection(rover.getRoverDirection().swipeLeft());
        assertEquals('W', rover.getRoverDirection().getDirectionAsChar());
    }

    @org.junit.Test
    public void hasCoordinateXStartedAsZero(){
        Rover rover = new Rover();
        assertEquals(0, rover.getRoverPosition().getPosX());
    }

    @org.junit.Test
    public void hasCoordinateYStartedAsZero(){
        Rover rover = new Rover();
        assertEquals(0, rover.getRoverPosition().getPosY());
    }

    @org.junit.Test
    public void moveForwardsAsNorth(){
        Rover rover = new Rover(new char[]{'F','F'});
        rover.interpretCommands();
        assertEquals(0, rover.getRoverPosition().getPosX());
        assertEquals(2, rover.getRoverPosition().getPosY());
    }

    @org.junit.Test
    public void swipeNorthDirectionToLeftAndMoveForwards(){
        Rover rover = new Rover(new char[]{'L','F'});
        rover.interpretCommands();
        assertEquals('W', rover.getRoverDirection().getDirectionAsChar());
        assertEquals(10, rover.getRoverPosition().getPosX());
    }

    @org.junit.Test
    public void swipeNorthDirectionToRightAndMoveForwards(){
        Rover rover = new Rover(new char[]{'R','F'});
        rover.interpretCommands();
        assertEquals('E', rover.getRoverDirection().getDirectionAsChar());
        assertEquals(1, rover.getRoverPosition().getPosX());

    }

    @org.junit.Test
    public void swipeNorthDirectionToRightRightAndMoveForwards(){
        Rover rover = new Rover(new char[]{'R','R','F'});
        rover.interpretCommands();
        assertEquals('S', rover.getRoverDirection().getDirectionAsChar());
        assertEquals(10, rover.getRoverPosition().getPosY());
    }

    @org.junit.Test
    public void swipeNorthDirectionToRightRightAndMoveBackwards(){
        Rover rover = new Rover(new char[]{'R','R','B'});
        rover.interpretCommands();
        assertEquals('S', rover.getRoverDirection().getDirectionAsChar());
        assertEquals(1, rover.getRoverPosition().getPosY());
    }

    @org.junit.Test
    public void isNextForwardMoveOutOfMars(){
        Rover rover = new Rover(new char[]{'L','F'});
        rover.interpretCommands();
        assertEquals(10,rover.getRoverPosition().getPosX());
        assertEquals(0,rover.getRoverPosition().getPosY());
    }

    @org.junit.Test
    public void isNextBackwardMoveOutOfMars(){
        Rover rover = new Rover(new char[]{'B'});
        rover.interpretCommands();
        assertEquals(0,rover.getRoverPosition().getPosX());
        assertEquals(10,rover.getRoverPosition().getPosY());
    }

    @org.junit.Test
    public void isNextForwardMoveAnObstacle(){
        Rover rover = new Rover(new char[]{'F','R','F'});
        rover.interpretCommands();
        assertEquals(0,rover.getRoverPosition().getPosX());
        assertEquals(1,rover.getRoverPosition().getPosY());

    }

    @org.junit.Test
    public void isNextBackwardMoveAnObstacle(){
        Rover rover = new Rover(new char[]{'F','L','B'});
        rover.interpretCommands();
        assertEquals(0,rover.getRoverPosition().getPosX());
        assertEquals(1,rover.getRoverPosition().getPosY());

    }
}
