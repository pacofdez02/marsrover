package org.ffernanl;

import org.ffernanl.Direction.Direction;
import org.ffernanl.Direction.North;


public class Rover{
    private Position roverPosition;
    private Direction roverDirection;

    private Mars mars;
    private char[] commands;


    public Rover(Mars mars, Position roverPosition, Direction roverDirection, char[] commands) {
        this.mars = mars;
        this.roverPosition = roverPosition;
        this.roverDirection = roverDirection;
        this.commands = commands;
    }

    public Rover(char[] commands){
        this.mars = new Mars();
        this.commands = commands;
        roverPosition = new Position(0,0);
        roverDirection = new North();

    }
    public Rover() {
        this.mars = new Mars();
        roverPosition = new Position(0,0);
        roverDirection = new North();
        this.commands = null;
    }


    public Position getRoverPosition() {
        return roverPosition;
    }

    public void setRoverPosition(Position roverPosition) {
        this.roverPosition = roverPosition;
    }

    public Direction getRoverDirection() {
        return roverDirection;
    }

    public void setRoverDirection(Direction roverDirection) {
        this.roverDirection = roverDirection;
    }

    public void interpretCommands(){
        for (char actual: commands) {
            if(actual == 'F') {
                if (!checkIfNextForwardMoveEqualsAnObstacle())
                    setRoverPosition(checkIfNextForwardMoveEqualsAnEdge());
            }
            else if(actual == 'B') {
                if (!checkIfNextBackwardMoveEqualsAnObstacle())
                    setRoverPosition(checkIfNextBackwardMoveEqualsAnEdge());
            }
            else if(actual == 'L')
                setRoverDirection(roverDirection.swipeLeft());
            else if(actual == 'R')
                setRoverDirection(roverDirection.swipeRight());


        }
    }



    public Position checkIfNextForwardMoveEqualsAnEdge() {

        Position newRoverPosition = roverDirection.moveForwards();
        int posX = newRoverPosition.getPosX();
        int posY = newRoverPosition.getPosY();
        return new Position(Math.floorMod(posX+ roverPosition.getPosX(), 11), Math.floorMod(posY+ roverPosition.getPosY(), 11));
    }

    public Position checkIfNextBackwardMoveEqualsAnEdge() {
        Position newRoverPosition = roverDirection.moveBackwards();
        int posX = newRoverPosition.getPosX();
        int posY = newRoverPosition.getPosY();
        return new Position(Math.floorMod(posX+ roverPosition.getPosX(), 11), Math.floorMod(posY+ roverPosition.getPosY(), 11));

    }

    public boolean checkIfNextForwardMoveEqualsAnObstacle(){
        Position newRoverPosition = checkIfNextForwardMoveEqualsAnEdge();
        for (Obstacle obstacle: mars.getObstacles()) {
            if(newRoverPosition.getPosX() == obstacle.getPosition().getPosX() && newRoverPosition.getPosY() == obstacle.getPosition().getPosY())
                return true;

        }
        return false;

    }

    public boolean checkIfNextBackwardMoveEqualsAnObstacle(){
        Position newRoverPosition = checkIfNextBackwardMoveEqualsAnEdge();
        for (Obstacle obstacle: mars.getObstacles()) {
            if(newRoverPosition.getPosX() == obstacle.getPosition().getPosX() && newRoverPosition.getPosY() == obstacle.getPosition().getPosY())
                return true;

        }
        return false;

    }
}
