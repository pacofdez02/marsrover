package org.ffernanl.Direction;

import org.ffernanl.Position;
import org.ffernanl.Rover;


public class North implements Direction{
    @Override
    public char getDirectionAsChar() {
        return 'N';
    }

    @Override
    public Position moveForwards() {
        return new Position(0,1);
    }

    @Override
    public Position moveBackwards() {
        return new Position(0,-1);
    }


    @Override
    public Direction swipeRight() {
        return new East();
    }

    @Override
    public Direction swipeLeft() {
        return new West();
    }
}
