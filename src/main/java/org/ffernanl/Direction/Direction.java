package org.ffernanl.Direction;

import org.ffernanl.Position;
import org.ffernanl.Rover;

public interface Direction {

    char getDirectionAsChar();
    Position moveForwards();
    Position moveBackwards();
    Direction swipeRight();
    Direction swipeLeft();
}
