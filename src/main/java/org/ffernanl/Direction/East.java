package org.ffernanl.Direction;

import org.ffernanl.Position;
import org.ffernanl.Rover;

public class East implements Direction{

    @Override
    public char getDirectionAsChar() {
        return 'E';
    }
    @Override
    public Position moveForwards() {
        return new Position(1,0);
    }

    @Override
    public Position moveBackwards() {
        return new Position(-1,0);
    }

    @Override
    public Direction swipeRight() {
        return new South();
    }

    @Override
    public Direction swipeLeft() {
        return new North();
    }
}
