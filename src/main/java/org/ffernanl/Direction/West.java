package org.ffernanl.Direction;

import org.ffernanl.Position;
import org.ffernanl.Rover;

public class West implements Direction{
    @Override
    public char getDirectionAsChar() {
        return 'W';
    }
    @Override
    public Position moveForwards() {
        return new Position(-1,0);
    }

    @Override
    public Position moveBackwards() {
        return new Position(1,0);
    }

    @Override
    public Direction swipeRight() {
        return new North();
    }

    @Override
    public Direction swipeLeft() {
        return new South();
    }
}
