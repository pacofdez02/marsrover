package org.ffernanl.Direction;

import org.ffernanl.Position;
import org.ffernanl.Rover;

public class South implements Direction {
    @Override
    public char getDirectionAsChar() {
        return 'S';
    }
    @Override
    public Position moveForwards() {
        return new Position(0,-1);
    }

    @Override
    public Position moveBackwards() {
        return new Position(0,1);
    }

    @Override
    public Direction swipeRight() {
        return new West();
    }

    @Override
    public Direction swipeLeft() {
        return new East();
    }
}
