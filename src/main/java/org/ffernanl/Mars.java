package org.ffernanl;

import java.util.Random;

public class Mars {
    private Obstacle[] obstacles;
    public static final Position positionMax = new Position(10,10);

    public Mars(){
        this.obstacles = new Obstacle[5];
        createObstaclesInMars();
    }

    public Obstacle[] getObstacles() {
        return obstacles;
    }

    public void setObstacles(Obstacle[] obstacles) {
        this.obstacles = obstacles;
    }

    public void createObstaclesInMars(){
        for (int i = 0; i < obstacles.length; i++) {
            //todo quitar esto y ponerlo random para acabar el ejercicio, está hecho así para comprobar más rápido el test
            obstacles[i] = new Obstacle(new Position(1,1));

            /*int posX = (int)(Math.random()*10);
            int posY = (int)(Math.random()*10);
            obstacles[i] = new Obstacle(new Position(posX,posY));*/
        }
    }
}
